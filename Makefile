virtualenv:
	python3 -m venv virtualenv
	./virtualenv/bin/pip3 install -r requirements.txt

package:
	python3 setup.py sdist

publish:
	twine upload dist/*
